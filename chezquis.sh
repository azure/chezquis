#!/bin/sh

installpkgs() { pacman --noconfirm --needed -S "$@" ;}

prettypac() {
    sed -i "s/^#ParallelDownloads.*$/ParallelDownloads = 5/;s/^#Color$/Color/" /etc/pacman.conf
    grep -q "ILoveCandy" /etc/pacman.conf || sed -i "/^ParallelDownloads/a ILoveCandy" /etc/pacman.conf
}

smallswappie() { printf "vm.swappiness=10\n" > /etc/sysctl.d/99-swappiness.conf ;}

cfg_reflector() { installpkgs reflector &&
    printf -- "--save /etc/pacman.d/mirrorlist\n--country USA\n--protocol https\n--latest 10\n" > /etc/xdg/reflector/reflector.conf &&
    systemctl enable reflector.timer ;}

default_zsh() { installpkgs zsh && chsh -s /usr/bin/zsh ;}

mkusr() { installpkgs zsh opendoas &&
    useradd -m -s /usr/bin/zsh $NAME &&
    printf -- "permit persist $NAME as root\npermit nopass root as $NAME\n" > /etc/doas.conf &&
    echo "$NAME:$PASS" | chpasswd &&
    echo "$NAME ALL=(ALL:ALL) ALL" > /etc/sudoers.d/azure ;}

import_gpg() { installpkgs gnupg &&
    cp $PRIVATE_KEY /tmp/ &&
    chown $NAME:$NAME /tmp/$(basename $PRIVATE_KEY) &&
    doas -u $NAME gpg --pinentry-mode=loopback /tmp/$(basename $PRIVATE_KEY) &&
    doas -u $NAME gpg --pinentry-mode=loopback --import /tmp/$(basename $PRIVATE_KEY .gpg) &&
    clear &&
    printf "\033[1m\033[31missue 'trust' and then 'quit'\n\033[0m" &&
    doas -u $NAME gpg --edit-key "$RECIPIENT" ;}

import_ssh() { installpkgs openssh &&
    doas -u $NAME mkdir -p /home/$NAME/.ssh &&
    cp $ID_RSA /tmp/$(basename $ID_RSA) &&
    chown $NAME:$NAME /tmp/$(basename $ID_RSA) &&
    doas -u $NAME sh -c "{ gpg --pinentry-mode=loopback -d /tmp/$(basename $ID_RSA) ;} > /home/$NAME/.ssh/id_rsa" &&
    chmod 600 /home/$NAME/.ssh/id_rsa &&
    doas -u $NAME sh -c "{ ssh-keygen -y -f /home/$NAME/.ssh/id_rsa ;} > /home/$NAME/.ssh/id_rsa.pub" ;}

mkaur() { ! command -v paru >/dev/null 2>&1 &&
    cd /tmp &&
    curl -O https://aur.archlinux.org/cgit/aur.git/snapshot/paru.tar.gz &&
    doas -u $NAME tar -xvf paru.tar.gz &&
    cd paru &&
    doas -u $NAME makepkg --noconfirm -si &&
    sed -i 's/^#\[bin\]/\[bin\]/g ; s/^#Sudo = doas/Sudo = doas/g ; s/^SudoLoop/#SudoLoop/g' /etc/paru.conf ;}

deploy_chezmoi() { installpkgs chezmoi && doas -u $NAME chezmoi init --apply $CHEZMOI_REPO ;}

NAME="brian"
RECIPIENT="bfitzpat.math.duke@gmail.com"
PRIVATE_KEY="/run/media/root/sandisk/private.key.gpg"
ID_RSA="/run/media/root/sandisk/id_rsa.gpg"
CHEZMOI_REPO="git@gitlab.oit.duke.edu:azure/dotfiles.git"

while getopts ":n:p:r:k:i:c:" opt; do
    case "${opt}" in
        n) NAME=${OPTARG} ;;
        p) PASS=${OPTARG} ;;
        r) RECIPIENT=${OPTARG} ;;
        k) PRIVATE_KEY=${OPTARG} ;;
        i) ID_RSA=${OPTARG} ;;
        c) CHEZMOI_REPO=${OPTARG} ;;
    esac
done

[ -z "$NAME" ] && echo "must set NAME with -n" && exit
[ -z "$PASS" ] && echo "must set PASS with -p" && exit
[ -z "$RECIPIENT" ] && echo "must set RECIPIENT with -r" && exit
[ -z "$PRIVATE_KEY" ] && echo "must set PRIVATE_KEY with -k" && exit
[ -z "$ID_RSA" ] && echo "must set ID_RSA with -i" && exit
[ -z "$CHEZMOI_REPO" ] && echo "must set CHEZMOI_REPO with -c" && exit

[ ! -f "$PRIVATE_KEY" ] && echo "$PRIVATE_KEY does not exist!" && exit
[ ! -f "$ID_RSA" ] && echo "$ID_RSA does not exist!" && exit

prettypac
smallswappie
cfg_reflector
default_zsh
mkusr
import_gpg
import_ssh
mkaur
deploy_chezmoi
